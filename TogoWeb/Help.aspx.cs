﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.Net.Mail;

namespace TogoWeb
{
    public partial class Help : System.Web.UI.Page
    {
        static TraceSource trace;

        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");
        }

        protected void Button_Help_Send_Click(object sender, EventArgs e)
        {
            //Check if there is text in the textboxes
            Label_Help_Error_Name.Visible = false;
            Label_Help_Error_Email.Visible = false;
            Label_Help_Error_Subject.Visible = false;
            Label_Help_Error_Message.Visible = false;

            int ck = 0;
            if (TextBox_Help_Name.Text == "")
            {
                Label_Help_Error_Name.Visible = true;
                ck = 1;
            }
            if (TextBox_Help_Email.Text == "")
            {
                Label_Help_Error_Email.Visible = true;
                ck = 1;
            }
            if (TextBox_Help_Subject.Text == "")
            {
                Label_Help_Error_Subject.Visible = true;
                ck = 1;
            }
            if (TextBox_Help_Message.Text == "")
            {
                Label_Help_Error_Message.Visible = true;
                ck = 1;
            }
            if (ck == 1)
            {
                return;
            }

            string addressTO = "";
            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    //var query = from c in context.ContactLists
                    //            where c.TechSupport == true
                    //            select c;

                    var query = from c in context.Users
                                where c.Permission_TechSupport == true
                                select c;

                    if (query.Count() > 0)
                    {
                        //foreach (ContactList cc in query)
                        foreach (User cc in query)
                        {
                            if (addressTO == "")
                            {
                                addressTO = cc.Email;
                            }
                            else
                            {
                                addressTO = addressTO + "," + cc.Email;
                            }
                        }

                        bool success = SendEmail(TextBox_Help_Email.Text, TextBox_Help_Subject.Text, TextBox_Help_Message.Text, TextBox_Help_Name.Text, addressTO);

                        if (success == true)
                        {
                            Panel_Help_Email.Visible = false;
                            Label_Help_Sent.Visible = true;
                            Button_Help_Sent_OK.Visible = true;
                        }
                        if (success == false)
                        {
                            Panel_Help_Email.Visible = false;
                            Label_Help_Sent_Error.Visible = true;
                            Label_Help_Sent_ErrorContact.Visible = true;
                            Button_Help_Sent_OK.Visible = true;
                        }
                    }
                    else
                    {
                        Panel_Help_Email.Visible = false;
                        Label_Help_Sent_Error.Visible = true;
                        Label_Help_Sent_ErrorContact.Visible = true;
                        Button_Help_Sent_OK.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Reset_Email_Form()
        {
            Label_Help_Error_Name.Visible = false;
            Label_Help_Error_Email.Visible = false;
            Label_Help_Error_Subject.Visible = false;
            Label_Help_Error_Message.Visible = false;
            TextBox_Help_Name.Text = "";
            TextBox_Help_Email.Text = "";
            TextBox_Help_Subject.Text = "";
            TextBox_Help_Message.Text = "";

            Label_Help_Sent.Visible = false;
            Label_Help_Sent_Error.Visible = false;
            Label_Help_Sent_ErrorContact.Visible = false;
            Button_Help_Sent_OK.Visible = false;
            Panel_Help_Email.Visible = true;
        }

        public static bool SendEmail(string addressFROM, string subject, string msgBody, string name, string addressTO)
        {
            msgBody = "This message was sent from the Togo Code for Resilience web interface. You received this message because you are identified as part of the technical support staff.\n\n" + msgBody + "\n\nMessage from: " + name + " " + addressFROM;
            bool success = false;
            MailMessage message = new MailMessage();
            message.From = new MailAddress("donotreply@togocfr.com");  //HARDWIRE: for demonstration only - replace with another email ("soporte@gmail.com") // new MailAddress(addressFROM); 

            try
            {
                message.ReplyTo = new MailAddress(addressFROM);
                message.To.Add(addressTO);  // send to togo support staff
                message.Subject = subject;
                message.Body = msgBody;
            }
            catch
            {
                success = false;
                return success;
            }

            message.IsBodyHtml = false;
            message.Priority = MailPriority.Normal;

            using (SmtpClient SmtpMail = new SmtpClient())
            {
                SmtpMail.Host = "smtp.gmail.com";
                SmtpMail.Port = 587;
                SmtpMail.EnableSsl = true;
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = new System.Net.NetworkCredential("soporte@climagro.com.ar", "Tecmes10"); // HARDWIRE - for demonstration only - replace witha nother network credential ("soporte@climagro.com.ar", "Tecmes10")
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                try
                {
                    SmtpMail.Send(message);
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    trace.TraceEvent(TraceEventType.Error, 1, "Error sending email to support " + addressTO);
                    trace.Flush();
                }
            }
            return success;
        }

        protected void Button_Help_Sent_OK_Click(object sender, EventArgs e)
        {
            Reset_Email_Form();
            Panel_Help_Email.Visible = true;
        }
    }
}