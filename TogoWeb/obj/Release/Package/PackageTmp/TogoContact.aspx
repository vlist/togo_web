﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TogoContact.aspx.cs" Inherits="TogoWeb.TogoContact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
 p.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	}
        .auto-style2 {
            height: 33px;
        }
        .auto-style3 {
            height: 216px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" Text="Contact Information"></asp:Label>
                &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button_AddContact" runat="server" Font-Names="Calibri" OnClick="Button_AddContact_Click" Text="Add Contact" />
&nbsp;<asp:Label ID="Label_FirstName_Add" runat="server" Font-Names="Calibri" Text="First Name:" Visible="False"></asp:Label>
&nbsp;<asp:TextBox ID="TextBox_FirstName_Add" runat="server" Visible="False"></asp:TextBox>
&nbsp;<asp:Label ID="Label_LastName_Add" runat="server" Font-Names="Calibri" Text="Last Name:" Visible="False"></asp:Label>
&nbsp;<asp:TextBox ID="TextBox_LastName_Add" runat="server" Visible="False"></asp:TextBox>
&nbsp;<asp:Label ID="Label_Email_Add" runat="server" Font-Names="Calibri" Text="Email:" Visible="False"></asp:Label>
&nbsp;<asp:TextBox ID="TextBox_Email_Add" runat="server" Visible="False" Width="250px"></asp:TextBox>
&nbsp;<asp:Button ID="Button_AddContact_Save" runat="server" Font-Names="Calibri" OnClick="Button_AddContact_Save_Click" Text="Save" Visible="False" />
&nbsp;<asp:Button ID="Button_AddContact_Cancel" runat="server" Font-Names="Calibri" OnClick="Button_AddContact_Cancel_Click" Text="Cancel" Visible="False" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label_FirstName_Error" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="Please enter the First Name. " Visible="False"></asp:Label>
                    <asp:Label ID="Label_LastName_Error" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="Please enter the Last Name. " Visible="False"></asp:Label>
                    <asp:Label ID="Label_Email_Error" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="Please enter the Email. " Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:GridView ID="GridView_ContactList" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="False">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_ID" runat="server" Enabled="False" Width="50px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_FirstName" runat="server" Enabled="False"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_LastName" runat="server" Enabled="False"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_Email" runat="server" Enabled="False" Width="250px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="Button_GridView_Edit" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Edit_Command" Text="Edit" />
                                    <asp:Button ID="Button_GridView_Save" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Save_Command" Text="Save" Visible="False" />
                                    &nbsp;<asp:Button ID="Button_GridView_Delete" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Delete_Command" Text="Delete" Visible="False" />
                                    &nbsp;<asp:Button ID="Button_GridView_Cancel" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Cancel_Command" Text="Cancel" Visible="False" />
                                    &nbsp;<asp:Label ID="Label_Delete_Check" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Are you sure you want to delete this contact?" Visible="False"></asp:Label>
                                    &nbsp;<asp:Button ID="Button_GridView_Delete_Yes" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Delete_Yes_Command" Text="Yes" Visible="False" />
                                    &nbsp;<asp:Button ID="Button_GridView_Delete_No" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Delete_No_Command" Text="No" Visible="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label_NoContacts" runat="server" Font-Names="Calibri" Text="There are no saved contacts" Visible="False" Font-Italic="True"></asp:Label>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
