﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Simulations.aspx.cs" Inherits="TogoWeb.Simulations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 108px;
        }
        .auto-style3 {
            height: 23px;
        }
        .auto-style7 {
            width: 267px;
        }
        .auto-style8 {
            height: 30px;
        }
        .auto-style10 {
            height: 26px;
        }
        .auto-style12 {
            height: 30px;
            }
        .auto-style13 {
            height: 26px;
            }
        .auto-style24 {
            height: 26px;
            }
        .auto-style25 {
            height: 24px;
        }
        .auto-style26 {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel1" runat="server" BorderColor="Black" BorderStyle="Double" Width="540px">
                                            &nbsp;&nbsp;<br />&nbsp;&nbsp;<asp:Label ID="Label28" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Most Recent Simulation"></asp:Label>
                                            &nbsp;<asp:Button ID="Button_Review" runat="server" BorderColor="#660066" BorderStyle="Double" Font-Bold="True" Font-Size="Medium" OnClick="Button_Review_Click" Text="Validate!" />
                                            &nbsp;<asp:TextBox ID="SelectedSim_TextBox_Header" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label29" runat="server" Font-Names="Calibri" Text="Simulation Period: "></asp:Label>
                                            <asp:Label ID="StartTime_TextHeader" runat="server" Font-Names="Calibri" Text="(Start Time)"></asp:Label>
                                            &nbsp;<asp:Label ID="Label30" runat="server" Font-Names="Calibri" Text="to "></asp:Label>
                                            <asp:Label ID="EndTime_TextHeader" runat="server" Font-Names="Calibri" Text="(End Time)"></asp:Label>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label31" runat="server" Font-Names="Calibri" Text="Automated Predicted Risk: "></asp:Label>
                                            <asp:Label ID="Risk_Text_Header" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                            &nbsp;<asp:Image ID="Image_Null_Header" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_A_Header" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                            <asp:Image ID="Image_B_Header" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_C_Header" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_D_Header" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_E_Header" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                            <br />
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label32" runat="server" Font-Names="Calibri" Text="Risk Predicted by Dam Operator:"></asp:Label>
                                            <asp:Label ID="Risk_Calc_Text_Header" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                            &nbsp;<asp:Image ID="Image_Null_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_A_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                            <asp:Image ID="Image_B_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" Width="21px" />
                                            <asp:Image ID="Image_C_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_D_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_E_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                            <br />
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label_NoSimulations_Header" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" Text="There are no past simulations." Visible="False"></asp:Label>
                                            <br />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style10">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Summary of Recent Simulations"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label_NoSimulations" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" Text="There are no past simulations." Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:GridView ID="Simulations_GridView" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="Id" HeaderText="ID" />
                                                <asp:BoundField DataField="StartTime" HeaderText="Simulation Period Start" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EndTime" HeaderText="Simulation Period End">
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                <asp:BoundField DataField="RunStartTime" HeaderText="Run Date" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RiskLetter" HeaderText="Automated Predicted Risk" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RiskDescription" HeaderText="Description" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image_Null" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" />
                                                        <asp:Image ID="Image_A" runat="server" Height="20px" ImageUrl="~/Images/red_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_B" runat="server" Height="20px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_C" runat="server" Height="20px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_D" runat="server" Height="20px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_E" runat="server" Height="20px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RiskLetterOpt" HeaderText="Risk Predicted by Dam Operator">
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RiskDescriptionOpt" HeaderText="Description" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image_Null_Opt" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" />
                                                        <asp:Image ID="Image_A_Opt" runat="server" Height="20px" ImageUrl="~/Images/red_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_B_Opt" runat="server" Height="20px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_C_Opt" runat="server" Height="20px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_D_Opt" runat="server" Height="20px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_E_Opt" runat="server" Height="20px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:Button ID="Simulation_Details_Button" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Details_Button_Command" Text="Details" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Validated?">
                                                    <ItemTemplate>
                                                        <asp:Button ID="Validated_Button" runat="server" BackColor="#00CC66" CommandArgument="<%# Container.DataItemIndex %>" Font-Bold="True" ForeColor="Black" OnCommand="Validated_Button_Command" Text="Yes/No" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 700">&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Simulation Details"></asp:Label>
                                        &nbsp;<asp:TextBox ID="SelectedSim_TextBox" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Font-Names="Calibri" Text="Simulation Period: "></asp:Label>
                                        <asp:Label ID="StartTime_Text" runat="server" Font-Names="Calibri" Text="(Start Time)"></asp:Label>
                                        &nbsp;<asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="to "></asp:Label>
                                        <asp:Label ID="EndTime_Text" runat="server" Font-Names="Calibri" Text="(End Time)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Font-Names="Calibri" Text="Status: "></asp:Label>
                                        <asp:Label ID="Status_Text" runat="server" Font-Names="Calibri" Text="(Status)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="Run Date:"></asp:Label>
                                        <asp:Label ID="RunStartTime_Text" runat="server" Font-Names="Calibri" Text="(Simulation Start Time)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        <asp:Label ID="Label5" runat="server" Font-Names="Calibri" Text="Automated Predicted Risk: "></asp:Label>
                                        <asp:Label ID="Risk_Text" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Font-Names="Calibri" Text="Description: "></asp:Label>
                                        <asp:Label ID="Description_Text" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label36" runat="server" Font-Names="Calibri" Text="Risk Predicted by Dam Operator:"></asp:Label>
                                        &nbsp;<asp:Label ID="Risk_Text_Dam" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label37" runat="server" Font-Names="Calibri" Text="Description: "></asp:Label>
                                        <asp:Label ID="Description_Text_Dam" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">
                                        <asp:Button ID="Ok_Button" runat="server" Height="26px" OnClick="Ok_Button_Click" Text="Ok" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Simulation Details - Downstream Nangbeto Dam"></asp:Label>
                                        &nbsp;<asp:TextBox ID="SelectedSim_TextBox_Calc" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                        &nbsp;<asp:TextBox ID="SelectedSc_TextBox" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label10" runat="server" Font-Names="Calibri" Text="Simulation Period: "></asp:Label>
                                        <asp:Label ID="StartTime_Text0" runat="server" Font-Names="Calibri" Text="(Start Time)"></asp:Label>
                                        &nbsp;<asp:Label ID="Label11" runat="server" Font-Names="Calibri" Text="to "></asp:Label>
                                        <asp:Label ID="EndTime_Text0" runat="server" Font-Names="Calibri" Text="(End Time)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        <asp:Label ID="Label12" runat="server" Font-Names="Calibri" Text="Automated Predicted Risk: "></asp:Label>
                                        &nbsp;</td>
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Font-Names="Calibri" Text="Risk Predicted by Dam Operator:" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Risk_Text0" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                        &nbsp;<asp:Image ID="Image_Null" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_A" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_B" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_C" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_D" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_E" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Risk_Calc_Text" runat="server" Font-Names="Calibri" Text="(Risk)" Visible="False"></asp:Label>
                                        &nbsp;<asp:Image ID="Image_Null_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_A_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_B_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_C_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_D_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_E_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Description_Text0" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                                        &nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Description_Calc_Text" runat="server" Font-Names="Calibri" Text="(Description)" Visible="False"></asp:Label>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3" colspan="2">
                                        <asp:Button ID="Cancel_Button" runat="server" Font-Bold="True" Height="26px" OnClick="Cancel_Button_Click" Text="Ok" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label14" runat="server" Font-Names="Calibri" Text="Nangbeto Dam Water Level [m]: "></asp:Label>
                                        <asp:Label ID="WaterLevel_Text" runat="server" Font-Names="Calibri" Text="(Water Level)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label15" runat="server" Font-Names="Calibri" Text="Nangbeto Dam Flows [m3/s]:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style24" colspan="2">
                                        <asp:Label ID="Label35" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text=" " Width="75px"></asp:Label>
                                        <asp:Label ID="Label34" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text="Inflow" Width="160px"></asp:Label>
                                        <asp:Label ID="Label26" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text="Outflow - Automated Prediction" Width="160px"></asp:Label>
                                        <asp:Label ID="Label33" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text="Outflow - GloFAS Prediction" Width="160px"></asp:Label>
                                        <asp:Label ID="Label27" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text="Outflow - User Prediction" Visible="False" Width="160px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style24" colspan="2"><asp:Label ID="Label16" runat="server" Font-Names="Calibri" Text="Day - 2:" Width="75px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus2_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus2_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus2_Label_LOFA" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowMinus2_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style12">
                                        <asp:Label ID="Label17" runat="server" Font-Names="Calibri" Text="Day  - 1:" Width="75px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus1_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus1_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus1_Label0" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowMinus1_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style13">
                                        <asp:Label ID="Label18" runat="server" Font-Names="Calibri" Text="Day 0:" Width="75px"></asp:Label>
                                        <asp:Label ID="FlowDay0_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDay0_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDay0_Label0" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:TextBox ID="Flow0_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style26">
                                        <asp:Label ID="Label19" runat="server" Font-Names="Calibri" Text="Day + 1:" Width="75px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus1_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus1_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus1_Label0" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowPlus1_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style26">
                                        <asp:Label ID="Label20" runat="server" Font-Names="Calibri" Text="Day + 2:" Width="75px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus2_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus2_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus2_Label0" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Not Available" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowPlus2_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style26" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;<asp:Button ID="AcceptSend_Button" runat="server" Font-Bold="True" Height="26px" OnClick="AcceptSend_Button_Click" Text="Accept and Send Communication" Width="237px" BackColor="#009900" ForeColor="Black" />
                                        &nbsp;<asp:Label ID="Label_AcceptSend_Alert_Confirm" runat="server" Font-Bold="True" Font-Names="Calibri" ForeColor="Red" Text="Are you sure you want to send risk communication?" Visible="False"></asp:Label>
                                        &nbsp;<asp:Button ID="AcceptSend_Button_ConfirmYes" runat="server" Font-Bold="True" Height="26px" OnClick="Send_Button_Click" Text="Yes" Visible="False" />
                                        &nbsp;<asp:Button ID="AcceptSend_Button_ConfirmCancel" runat="server" Font-Bold="True" Height="26px" OnClick="AcceptSend_Button_Click_Cancel" Text="Cancel" Visible="False" />
                                        &nbsp;<asp:Button ID="Modify_Button" runat="server" Font-Bold="True" Height="26px" OnClick="Modify_Button_Click" Text="Modify Flows to Calculate Downstream Risk" Width="302px" BackColor="Yellow" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3" colspan="2">&nbsp;<asp:Button ID="Calculate_Button" runat="server" Font-Bold="True" Height="26px" OnClick="Calculate_Button_Click" Text="Calculate!" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label21" runat="server" Font-Names="Calibri" Text="Calculated Downstream Risk: " Visible="False"></asp:Label>
                                        &nbsp; 
                                        <asp:TextBox ID="Calc_ScenarioID" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Risk_Calculated_Text" runat="server" Font-Names="Calibri" Text="(Risk)" Visible="False"></asp:Label>
                                        &nbsp;<asp:Image ID="Image_Null_Calc" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_A_Calc" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_B_Calc" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_C_Calc" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_D_Calc" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_E_Calc" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style25">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Description_Text_Calc" runat="server" Font-Names="Calibri" Text="(Description)" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="SaveSend_Button" runat="server" Font-Bold="True" Height="26px" OnClick="SaveSend_Button_Click" Text="Validate and Send Communication" Visible="False" Width="236px" />
                                        &nbsp;<asp:Label ID="Label_SaveSend_Alert_Confirm" runat="server" Font-Bold="True" Font-Names="Calibri" ForeColor="Red" Text="Are you sure you want to send risk communication?" Visible="False"></asp:Label>
                                        &nbsp;<asp:Button ID="SaveSend_Button_ConfirmYes" runat="server" Font-Bold="True" Height="26px" OnClick="Send_Button_Click" Text="Yes" Visible="False" />
                                        &nbsp;<asp:Button ID="SaveSend_Button_ConfirmCancel" runat="server" Font-Bold="True" Height="26px" OnClick="SaveSend_Button_Click_Cancel" Text="Cancel" Visible="False" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
