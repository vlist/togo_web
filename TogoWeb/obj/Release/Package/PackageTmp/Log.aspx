﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Log.aspx.cs" Inherits="TogoWeb.Log" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 29px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" Text="Observation Log"></asp:Label>
                &nbsp;<asp:TextBox ID="TextBox_LogID" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button_AddObservation" runat="server" Font-Names="Calibri" OnClick="Button_AddObservation_Click" Text="Add Observation" />
&nbsp;<asp:Button ID="Button_Add_Save" runat="server" Font-Names="Calibri" OnClick="Button_Add_Save_Click" Text="Save" Visible="False" />
&nbsp;<asp:Button ID="Button_Add_Cancel" runat="server" Font-Names="Calibri" OnClick="Button_Add_Cancel_Click" Text="Cancel" Visible="False" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel1" runat="server">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_AddObservation" runat="server" Visible="False" Width="1003px">
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style2">
                                    <asp:Label ID="Label_Add_Calendar" runat="server" Font-Names="Calibri" Text="Date of Observation:"></asp:Label>
                                    &nbsp;<asp:Calendar ID="Calendar_Add" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px">
                                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                        <NextPrevStyle VerticalAlign="Bottom" />
                                        <OtherMonthDayStyle ForeColor="Gray" />
                                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                        <SelectorStyle BackColor="#CCCCCC" />
                                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <WeekendDayStyle BackColor="#FFFFCC" />
                                    </asp:Calendar>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2">
                                    <asp:Label ID="Label_Add_Location" runat="server" Font-Names="Calibri" Text="Location: "></asp:Label>
                                    <asp:DropDownList ID="DropDownList_Location" runat="server" AutoPostBack="True" Font-Names="Calibri" OnSelectedIndexChanged="DropDownList_Location_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    &nbsp;<asp:TextBox ID="TextBox_AddLocation" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Button ID="Button_AddLocation_Save" runat="server" Font-Names="Calibri" OnClick="Button_AddLocation_Save_Click" Text="Add Location" Visible="False" />
                                    &nbsp;<asp:Button ID="Button_AddLocation_Cancel" runat="server" Font-Names="Calibri" Height="25px" OnClick="Button_AddLocation_Cancel_Click" Text="Cancel" Visible="False" />
                                    &nbsp;<asp:Label ID="Label_Select_Location_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please select location" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Add_Location_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the location name" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Add_Location_ExistsError" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="This location already exists" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2">
                                    <asp:Label ID="Label_Add_Damage" runat="server" Font-Names="Calibri" Text="Damage Information:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="CheckBox_DamageNoDetail" runat="server" Font-Names="Calibri" Text="No Detail" />
                                    &nbsp;&nbsp;&nbsp;&nbsp; </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:CheckBox ID="CheckBox_DamageCrops" runat="server" Font-Names="Calibri" Text="Crops" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_DamageProp" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_DamageProp_CheckedChanged" Text="Property" />
                                    &nbsp;<asp:Label ID="Label_nProperty" runat="server" Font-Names="Calibri" Text="- Number of Households with Damage:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nProperty" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_nProperty_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the number of households with damage" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_PeopleDisplaced" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_PeopleDisplaced_CheckedChanged" Text="People Displaced" />
                                    &nbsp;<asp:Label ID="Label_nDisplaced" runat="server" Font-Names="Calibri" Text="- Number of People Displaced:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nDisplaced" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_nDisplaced_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the number of people displaced" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_Deaths" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_Deaths_CheckedChanged" Text="Deaths" />
                                    &nbsp;<asp:Label ID="Label_nDeaths" runat="server" Font-Names="Calibri" Text="- Number of Deaths:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nDeaths" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_nDeaths_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the number of deaths" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_Other" runat="server" Font-Names="Calibri" Text="Other" />
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label_Add_Description" runat="server" Font-Names="Calibri" Text="Description: "></asp:Label>
                                    <asp:TextBox ID="TextBox_Description" runat="server" Font-Names="Calibri" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_Description_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please describe the incident and damages" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Description_Char_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Text exceeds maximum number of allowed characters" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label_Add_Action" runat="server" Font-Names="Calibri" Text="Action: "></asp:Label>
                                    <asp:TextBox ID="TextBox_Action" runat="server" Font-Names="Calibri" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                    <asp:Label ID="Label_Action_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please summarize the actions performed" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Action_Char_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Text exceeds maximum number of allowed characters" Visible="False"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label_GridView_None" runat="server" Font-Italic="True" Font-Names="Calibri" Text="No Observations Recorded" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView_Log" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="DateTimeUse" HeaderText="Date" />
                            <asp:BoundField DataField="Location" HeaderText="Location" />
                            <asp:BoundField HeaderText="Damage:" />
                            <asp:CheckBoxField DataField="DamageNoDetail" HeaderText="No Detail">
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageCrops" HeaderText="Crops">
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageProperty" HeaderText="Property">
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageDeaths" HeaderText="Deaths">
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageOther" HeaderText="Other">
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:TemplateField HeaderText="Description">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_Description" runat="server" Enabled="False" Font-Names="Calibri" TextMode="MultiLine" Width="300px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_Action" runat="server" Enabled="False" Font-Names="Calibri" TextMode="MultiLine" Width="300px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:Button ID="Button_GridView_Log_Edit" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Log_Command" Text="Edit" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
