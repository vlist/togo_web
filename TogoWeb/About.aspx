﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="TogoWeb.About" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
 p.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	}
        .auto-style2 {
            height: 33px;
        }
        .auto-style4 {
            height: 132px;
        }
        .auto-style5 {}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                    <table class="auto-style1">
                        <tr>
                            <td class="auto-style5">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" Text="About"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style5">
                                <asp:Image ID="Image1" runat="server" Height="266px" ImageUrl="~/Images/Logos for FUNES website.png" Width="562px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                    <p class="MsoNormal">
                        Provisional text:</p>
                    <p class="MsoNormal">
                        This site provides a summary of past and ongoing simulations in Togo’s Mono River downstream of the Nangbeto Dam, as well as measured precipitation, flow rate, and water levels.
                    </p>
                    <p class="MsoNormal">
                        The web interface was developed for a project supported by Code for Resilience (CfR), Global Facility for Disaster Reduction and Recovery (GFDRR).
                    </p>
                    <p class="MsoNormal">
                        The main objective of this project is to forecast flood conditions in riverine communities downstream of the Nagbeto Dam and activate a set of pre-determined disaster preparedness procedures.
                    </p>
                    <p class="MsoNormal">
                        Users with a login can upload data records. Administrative users can configure user profiles and manage alerts corresponding to different flood scenarios.</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
