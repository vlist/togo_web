﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="TogoWeb.Help" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" Text="Help Information"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label_ContactUs" runat="server" Font-Names="Calibri" Text="Need help? Send a question:" Font-Italic="False" Font-Bold="True" Font-Underline="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label_Help_Sent" runat="server" Font-Names="Calibri" Text="Your message was sent. Support will contact you soon." Visible="False" Font-Italic="False"></asp:Label>
                &nbsp;<asp:Label ID="Label_Help_Sent_Error" runat="server" Font-Names="Calibri" Text="Your message could not be sent." Visible="False" Font-Italic="False"></asp:Label>
                &nbsp;<asp:Label ID="Label_Help_Sent_ErrorContact" runat="server" Font-Names="Calibri" Text="Please contact an administrator to identify the technical support contact(s). " Visible="False" Font-Italic="False"></asp:Label>
                &nbsp;<asp:Button ID="Button_Help_Sent_OK" runat="server" Font-Names="Calibri" OnClick="Button_Help_Sent_OK_Click" Text="Ok" Visible="False" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Panel ID="Panel_Help_Email" runat="server" Height="309px">
                        <asp:Label ID="Label_Help_Name" runat="server" Font-Names="Calibri" Text="Name:" Font-Italic="False"></asp:Label>
                        <br />
                        <asp:TextBox ID="TextBox_Help_Name" runat="server" Width="300px"></asp:TextBox>
                        &nbsp;<asp:Label ID="Label_Help_Error_Name" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter your name" Visible="False"></asp:Label>
                        <br />
                        <asp:Label ID="Label_Help_Email" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Email Address: "></asp:Label>
                        <br />
                        <asp:TextBox ID="TextBox_Help_Email" runat="server" Width="300px"></asp:TextBox>
                        &nbsp;<asp:Label ID="Label_Help_Error_Email" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter your email address" Visible="False"></asp:Label>
                        <br />
                        <asp:Label ID="Label_Help_Subject" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Subject: "></asp:Label>
                        <br />
                        <asp:TextBox ID="TextBox_Help_Subject" runat="server" Width="300px"></asp:TextBox>
                        &nbsp;<asp:Label ID="Label_Help_Error_Subject" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the subject " Visible="False"></asp:Label>
                        <br />
                        <asp:Label ID="Label_Help_Message" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Message: "></asp:Label>
                        <br />
                        <asp:TextBox ID="TextBox_Help_Message" runat="server" Height="116px" TextMode="MultiLine" Width="400px"></asp:TextBox>
                        <asp:Label ID="Label_Help_Error_Message" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter your question" Visible="False"></asp:Label>
                        <br />
                        <asp:Button ID="Button_Help_Send" runat="server" Font-Names="Calibri" OnClick="Button_Help_Send_Click" Text="Submit" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
