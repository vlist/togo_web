﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="TogoWeb.Users" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 25px;
        }
        .auto-style3 {
            height: 30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Manage Users"></asp:Label>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button_User_Add" runat="server" OnClick="Button_User_Add_Click" Text="Add User" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridView_Users" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox_ID" runat="server"></asp:TextBox>
                                                        <br />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Username" HeaderText="Username" />
                                                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                                                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                                                <asp:TemplateField HeaderText="Admin Privileges">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_Permission" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dam Operator Privileges">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_PermissionOpt" runat="server" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Red Cross Privileges">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_PermissionRedCross" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Technical Support">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_TechSupport" runat="server" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Email" HeaderText="Email" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="Button_GridView_Edit" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_GridView_Edit_Click" Text="Edit" />
                                                        &nbsp;<asp:Button ID="Button_GridView_Remove" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_GridView_Remove_Click" Text="Remove" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td class="auto-style3">
                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Add/Edit User"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="TextBox_AddUser_ID" runat="server" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Username:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_Username" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_Username" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the username" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Password:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_Password" runat="server" TextMode="Password"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_Password" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the password" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="First Name:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_FirstName" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_FirstName" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the first name" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Last Name:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_LastName" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_LastName" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the last name" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Email Address:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_Email" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_Email" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please enter the email address" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" Text="Enter more than one email by separating addresses with semicolon (ex: user@email.com;user2@email.com)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Administrative Privileges:"></asp:Label>
                                        &nbsp;<asp:CheckBox ID="CheckBox_AddUser_Permission" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="Label10" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Dam Operator Privileges: "></asp:Label>
                                        <asp:CheckBox ID="CheckBox_AddUser_PermissionOpt" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="Label11" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Red Cross Privileges: "></asp:Label>
                                        <asp:CheckBox ID="CheckBox_AddUser_PermissionRedCross" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="Label12" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Technical Support: "></asp:Label>
                                        <asp:CheckBox ID="CheckBox_AddUser_TechnicalSupport" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="Label_Warning_NoAdminPriv" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" Text="Please contact an administrator to edit user privileges" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button_User_Save" runat="server" OnClick="Button_User_Save_Click" Text="Save" />
                                        &nbsp;<asp:Button ID="Button_User_Cancel" runat="server" OnClick="Button_User_Cancel_Click" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            </table>
    
    </div>
    </form>
</body>
</html>
