﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddDataDam.aspx.cs" Inherits="TogoWeb.AddDataDam" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                                        <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Add Data Record at Nangbeto Dam" Font-Underline="True"></asp:Label>
                                    &nbsp;<asp:Label ID="Label28" runat="server" Font-Names="Calibri" Text="To add data at a different station, please select from the menu bar &quot;HydroMet Data / Add&quot;" Font-Italic="True" Font-Size="Small"></asp:Label>
                                                        </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                                                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Data Station:"></asp:Label>
                                                        &nbsp;<asp:Label ID="Label27" runat="server" Font-Names="Calibri" Text="Nangbeto Dam"></asp:Label>
                                                        &nbsp;</td>
            </tr>
            <tr>
                <td>
                                                        <asp:Label ID="Label19" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                                                        &nbsp;<asp:Label ID="Label24" runat="server" Font-Names="Calibri" Text="Inflow" Width="90px"></asp:Label>
                                                        &nbsp;
                                                        <asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="Value: "></asp:Label>
                                                        <asp:TextBox ID="Value_TextBox_Var1" runat="server" Font-Names="Calibri"></asp:TextBox>
                                                        <asp:Label ID="Label_Units1" runat="server" Font-Names="Calibri" Text="m3/s" Visible="False"></asp:Label>
                                                        &nbsp;<asp:Label ID="Value_Check_Label1" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="Please enter a value." Visible="False"></asp:Label>
                                                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                                                        <asp:Label ID="Label20" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                                                        &nbsp;<asp:Label ID="Label25" runat="server" Font-Names="Calibri" Text="Outflow" Width="90px"></asp:Label>
                                                        &nbsp;
                                                        <asp:Label ID="Label21" runat="server" Font-Names="Calibri" Text="Value: "></asp:Label>
                                                        <asp:TextBox ID="Value_TextBox_Var2" runat="server" Font-Names="Calibri"></asp:TextBox>
                                                        <asp:Label ID="Label_Units2" runat="server" Font-Names="Calibri" Text="m3/s" Visible="False"></asp:Label>
                                                        &nbsp;<asp:Label ID="Value_Check_Label2" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="Please enter a value." Visible="False"></asp:Label>
                                                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                                                        <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                                                        &nbsp;<asp:Label ID="Label26" runat="server" Font-Names="Calibri" Text="Water Level" Width="90px"></asp:Label>
                                                        &nbsp;
                                                        <asp:Label ID="Label23" runat="server" Font-Names="Calibri" Text="Value: "></asp:Label>
                                                        <asp:TextBox ID="Value_TextBox_Var3" runat="server" Font-Names="Calibri"></asp:TextBox>
                                                        <asp:Label ID="Label_Units3" runat="server" Font-Names="Calibri" Text="m, MSL" Visible="False"></asp:Label>
                                                        &nbsp;<asp:Label ID="Value_Check_Label3" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="Please enter a value." Visible="False"></asp:Label>
                                                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                                                        <asp:Calendar ID="Calendar_Add" runat="server"></asp:Calendar>
                                                        <asp:Label ID="Date_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="Please select a date" Visible="False"></asp:Label>
                                                    </td>
            </tr>
            <tr>
                <td>
                                                        <asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="Hour: "></asp:Label>
                                                        <asp:TextBox ID="Hour_TextBox" runat="server" Font-Names="Calibri">6</asp:TextBox>
                                                        <asp:Label ID="Label9" runat="server" Font-Names="Calibri" Text="Minute: "></asp:Label>
                                                        <asp:TextBox ID="Minute_TextBox" runat="server" Font-Names="Calibri" style="margin-top: 0px">0</asp:TextBox>
                                                        <asp:Label ID="Time_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="Please enter a time." Visible="False"></asp:Label>
                                                    </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                                                        <asp:Button ID="Save_Button" runat="server" OnClick="Button_Save_Click" Text="Save" />
                                                        <asp:Button ID="Button_Reset" runat="server" OnClick="Button_Reset_Click" Text="Clear" />
                                                    &nbsp;<asp:Label ID="Time_Check_Label_Warning" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="Values are missing for one or more variables. Do you want to continue? " Visible="False"></asp:Label>
                                                        <asp:Button ID="Save_Button_Yes" runat="server" OnClick="Button_Yes_Click" Text="Yes" Visible="False" />
                                                        &nbsp;<asp:Button ID="Save_Button_No" runat="server" OnClick="Button_No_Click" Text="No" Visible="False" />
                                                        </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
